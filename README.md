# Asteroid Server

This is the backend for my bachelor thesis project: An IPFS-based cloud clipboard manager.

Most of the project file structure is derived from https://www.wolfe.id.au/2020/03/10/how-do-i-structure-my-go-project/,
Accessed on 2022-08-01.

## Running the Server

Start an IPFS instance or use an exisiting one. Enable the experimental pubsub feature.

```
docker run --name ipfs -d -p 0.0.0.0:5001:5001 ipfs/kubo:master-2022-07-06-9ce802f --migrate=true --agent-version-suffix=docker -enable-pubsub-experiment --enable-namesys-pubsub
```

Build the server executable.

```
go build -o ./asteroid-server gitlab.gwdg.de/v.mattfeld/asteroid-server/cmd/asteroid-api
```

Start the server

```
./asteroid-server --ipfs-url="http://localhost:5001"
```
